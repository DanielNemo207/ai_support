from support_handler.supporter import Supporter

siri = Supporter()
siri.greeting()

end = False

while end == False:
    listened_text = siri.listen()

    if listened_text == "bye":
        end = True

    if listened_text == None:
        siri.speak("I cannot understand")
    else:
        siri.speak(listened_text)