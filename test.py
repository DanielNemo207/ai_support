import speech_recognition as sr

r = sr.Recognizer()
r.energy_threshold = 400
with sr.Microphone(device_index = 0, sample_rate = 48000) as source:
    r.adjust_for_ambient_noise(source, duration=5)
    print('Say sth')
    audio = r.record(source, duration = 1)
with open("microphone-results.wav", "wb") as f:
    f.write(audio.get_wav_data())