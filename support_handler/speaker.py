import pyttsx3

from support_handler.utils.enum_dict import Gender

class Speaker(object):
    
    def __init__(self):
        self.engine = pyttsx3.init()
        self.set_voice(Gender.Female)


    def set_volumn(self, vol):
        self.engine.setProperty('volume',vol)

    def set_voice(self, voice_gender):
        voices = self.engine.getProperty('voices')

        if voice_gender == Gender.Male:
            voice_id = voices[0].id 
        else:
            voice_id = voices[1].id 

        self.engine.setProperty('voice',voice_id)

    def speak(self, text):
        print('BOT: {}'.format(text))
        self.engine.say(text)
        self.engine.runAndWait()