from support_handler.speaker import Speaker
from support_handler.listener import Listener

class Supporter(Listener, Speaker):

    def __init__(self):
        #super(Supporter, self).__init__()
        Listener.__init__(self)
        Speaker.__init__(self)
        self.name = "Alice"

    def greeting(self):
        greeting_text = "Hi my name is {}. How can I help you?".format(self.name)
        self.speak(greeting_text) 