import speech_recognition

class Listener(object):

    def __init__(self):
        self.recognizer = speech_recognition.Recognizer()
        self.recognizer.energy_threshold = 500

    def listen(self):
        with speech_recognition.Microphone() as mic:
            print('____________Listening')
            self.recognizer.adjust_for_ambient_noise(mic)
            audio = self.recognizer.listen(mic)
        try:
            print('--------------DETECTED AUDIO')
            listened_text = self.recognizer.recognize_google(audio)
        except:
            print('--------------NON-DETECTED AUDIO')
            listened_text = None
        
        print('User: {}'.format(listened_text))
        return listened_text
